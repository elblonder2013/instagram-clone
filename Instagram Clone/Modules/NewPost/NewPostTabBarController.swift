//
//  NewPostTabBarController.swift
//  Instagram Clone
//
//  Created by Developer on 27/08/2020.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit

class NewPostTabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //creando los controladores
        let tabFeed = FromLibraryController(nibName: "FromLibraryController", bundle: nil)
        let tabFeedBarItem = UITabBarItem(title: "Biblioteca", image: nil, selectedImage: nil)
        tabFeed.tabBarItem = tabFeedBarItem
        
        let tabSearch = FromPhotoController(nibName: "FromPhotoController", bundle: nil)
        let tabSearchBarItem = UITabBarItem(title: "Foto", image: nil, selectedImage: nil)
        tabSearch.tabBarItem = tabSearchBarItem
     
        self.viewControllers = [tabFeed,tabSearch]
        
        let newPostSearch = FromViewController(nibName: "FromViewController", bundle: nil)
        let newPostBarItem = UITabBarItem(title: "Video", image: nil, selectedImage: nil)
        newPostSearch.tabBarItem = newPostBarItem
        self.viewControllers = [tabFeed,tabSearch,newPostSearch]
    
        let fontAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20)]
        UITabBarItem.appearance().setTitleTextAttributes(fontAttributes, for: .normal)
     tabSearch.tabBarItem.imageInsets = UIEdgeInsets.init(top: -10,left: 0,bottom: -5,right: 0)
        self.viewControllers = [tabFeed,tabSearch,newPostSearch]
        
    }
    
    
    
}
