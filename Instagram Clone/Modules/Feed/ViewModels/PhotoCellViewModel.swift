
class PhotoCellViewModel {
    
    var displayMediaUrl:String
    
    init(media:Media){
        self.displayMediaUrl = media.displayUrl
    }
}
