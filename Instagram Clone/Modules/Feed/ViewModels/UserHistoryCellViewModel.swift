


class UserHistoryCellViewModel {

    var userName:String
    var userDisplayUrl:String
    var seen:Bool
    var currentUser:Bool
    
    init(history:History, currentUser:Bool) {
        self.seen = history.seen
        self.userName = history.userName
        self.userDisplayUrl = history.profilePicUrl
        self.currentUser = currentUser
        
    }
    
    
}
