//
//  PostCellViewModel.swift
//  Instagram Clone
//
//  Created by Alexei Pineda Caraballo on 29/08/2020.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit

class PostCellViewModel {
    
    
    var userName:String
    var profilePicUrl:String
    var numberOfMedia:Int
    var post:Post
    var likes:String
    var comments:String
    var createdAt:String
    var postSize:CGSize
    var attributtedCaptionString:NSMutableAttributedString
    
    
    //data for collectionView
    func mediaViewModelForIndex(index:Int)->PhotoCellViewModel{
        let media = post.mediaList[index]
        return media.isVideo ? VideoCellViewModel(media: media) : PhotoCellViewModel(media: media)
    }
    
    
    init(post:Post){
        self.post = post
        let firstMedia = post.mediaList.first!
        
        //configure ouput values
        self.userName = post.usernameAuthor
        self.profilePicUrl = post.profilePicUrl
        self.likes = "\(post.likesCount) Me gusta" 
        self.comments = "Ver los \(post.commentsCount) comentarios"
        self.createdAt = Date(milliseconds: post.createdAt).relativePast()
        self.numberOfMedia = post.mediaList.count
        self.postSize = CGSize(width: firstMedia.width, height: firstMedia.height)
        
        self.attributtedCaptionString = NSMutableAttributedString(string: post.usernameAuthor + " ", attributes: [NSAttributedString.Key.font : UIFont(name: "SFProText-Medium", size: 13)!,NSAttributedString.Key.foregroundColor:  #colorLiteral(red: 0.1980366409, green: 0.1980422437, blue: 0.1980392635, alpha: 1)])
        self.attributtedCaptionString.append(NSAttributedString(string: post.postCaption, attributes:[NSAttributedString.Key.font : UIFont(name: "SFProText-Regular", size: 13)!,NSAttributedString.Key.foregroundColor:  #colorLiteral(red: 0.1980366409, green: 0.1980422437, blue: 0.1980392635, alpha: 1)]))
        
    }
    
}
