

class FeedViewModel {
    
    var manager : FeedRepository
    var currentPage = 0
    var dataSource = [PostCellViewModel]()
    var historyList = [History]()
    
    var postsLenght:Int {
        return dataSource.count
    }
    
    var loadFeedSuccesBlock:()->() = {}
    var loadFeedSuccesFailed:()->() = {}
    
    
    
    init(manager:FeedRepository) {
        self.manager = manager
        
    }
    
    func viewModelForPostAtIndex(index:Int)->PostCellViewModel {
        return self.dataSource[index]
    }
    
    func viewModelForHistories()->HistoriesCellViewModel{
        return HistoriesCellViewModel(historyList: self.historyList)
        
    }
    
    func loadFeed(){
        self.manager.getFeedsAndHistoriesForHome { (posts, histories, errorMessage) in
            if let posts = posts{
                self.dataSource = posts.map({ (post) -> PostCellViewModel in
                    return PostCellViewModel(post: post)
                })
                
            }
            if let histories = histories{
                self.historyList = histories
            }
            self.loadFeedSuccesBlock()
        }
    }
}
