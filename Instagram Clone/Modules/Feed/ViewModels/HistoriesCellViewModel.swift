
class HistoriesCellViewModel {
    
    var dataSource = [UserHistoryCellViewModel]()
    
    func viewModelForHistoryAtIndex(index:Int)->UserHistoryCellViewModel{
        return dataSource[index]
    }
    
    func historiesNumber()->Int{
        return self.dataSource.count
    }
    
    init(historyList:[History]) {
        dataSource.append(UserHistoryCellViewModel(history: History(storyId: "", profilePicUrl: "https://scontent-mad1-1.cdninstagram.com/v/t51.2885-19/s320x320/118288715_730083487848533_605153727897411789_n.jpg?_nc_ht=scontent-mad1-1.cdninstagram.com&_nc_ohc=8699752Xp1YAX8bYgjS&oh=92c44a322401874456bdca0be43600aa&oe=5F74E31B", userName: "Tu historia", seen: true), currentUser: true))
        self.dataSource.append(contentsOf: historyList.map({  UserHistoryCellViewModel(history: $0, currentUser: false) }))
    }
    
    
}
