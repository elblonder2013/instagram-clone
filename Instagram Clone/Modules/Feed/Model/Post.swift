

struct Post:Codable {
    var postId:String
   
    var mediaList:[Media]
    var postCaption:String
    var usernameAuthor:String
    var profilePicUrl:String
    var likesCount:Int
    var commentsCount:Int
    var createdAt:Int
    
    /*
    var isVideo:Bool
    var width:Int
    var height:Int*/
    
}
