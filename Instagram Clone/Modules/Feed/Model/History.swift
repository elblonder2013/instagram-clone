
struct History : Codable{
    var storyId:String
    var profilePicUrl:String
    var userName:String
    var seen:Bool
}
