//
//  Media.swift
//  Instagram Clone
//
//  Created by Alexei Pineda Caraballo on 29/08/2020.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit

struct Media:Codable {
    var mediaId:String
    var isVideo:Bool
    var width:Int
    var height:Int
    var urlSource:String
    var displayUrl:String
}
