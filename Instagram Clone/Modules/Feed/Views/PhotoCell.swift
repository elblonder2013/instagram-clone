//
//  PhotoCell.swift
//  Instagram Clone
//
//  Created by Alexei Pineda Caraballo on 30/08/2020.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit
import SDWebImage
class PhotoCell: UICollectionViewCell {
    @IBOutlet weak var mediaImageView: UIImageView!
    func bind(viewModel:PhotoCellViewModel){
        mediaImageView.sd_setImage(with: URL(string: viewModel.displayMediaUrl)) { (image, erro, type, url) in
            self.mediaImageView.image = image
        }
    }

}
