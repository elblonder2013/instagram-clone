//
//  HistoriesCell.swift
//  Instagram Clone
//
//  Created by Alexei Pineda Caraballo on 30/08/2020.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit

class HistoriesCell: UITableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var viewModel:HistoriesCellViewModel!
    func bind(viewModel:HistoriesCellViewModel){
        self.viewModel = viewModel
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.register(UINib(nibName: "UserHistoryCell", bundle: nil), forCellWithReuseIdentifier: "UserHistoryCellId")
        
        
        
        
    }
    
}
extension HistoriesCell:UICollectionViewDelegateFlowLayout,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserHistoryCellId", for: indexPath) as! UserHistoryCell
        cell.bind(viewModel: viewModel.viewModelForHistoryAtIndex(index: indexPath.row))
       
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.historiesNumber()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 74, height: 104)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    
}
