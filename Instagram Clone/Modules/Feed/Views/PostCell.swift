
import UIKit
import SDWebImage
class PostCell: UITableViewCell {
    
    @IBOutlet weak var postCollectionView: UICollectionView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var commentsLabel: UIButton!
    @IBOutlet weak var captionLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var likesButton: UIButton!
    @IBOutlet weak var pageControll: UIPageControl!
    
    var viewModel:PostCellViewModel!
    
    func bind(viewModel:PostCellViewModel){
        self.viewModel = viewModel
        postCollectionView.register(UINib(nibName: "PhotoCell", bundle: nil), forCellWithReuseIdentifier: "PhotoCellId")
        self.userNameLabel.text = viewModel.userName
        self.profileImageView.sd_setImage(with: URL(string: viewModel.profilePicUrl)) { (image, erro, type, url) in
            self.profileImageView.image = image
        }
        
        
        self.postCollectionView.dataSource = self
        self.postCollectionView.delegate = self
        self.heightConstraint.constant = self.frame.size.width * viewModel.postSize.height / viewModel.postSize.width
        self.pageControll.numberOfPages = viewModel.numberOfMedia
        self.pageControll.currentPage = 0
        self.pageControll.isHidden = viewModel.numberOfMedia == 1
        self.likesButton.setTitle(viewModel.likes, for: .normal)
        self.commentsLabel.setTitle(viewModel.comments, for: .normal)
        self.dateLabel.text = viewModel.createdAt
        self.captionLabel.attributedText = viewModel.attributtedCaptionString
    }
    
    
    
}
extension PostCell:UICollectionViewDelegateFlowLayout,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCellId", for: indexPath) as! PhotoCell
        cell.bind(viewModel: viewModel.mediaViewModelForIndex(index: indexPath.row))
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.numberOfMedia
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageWidth = scrollView.frame.size.width;
        let page = Int(floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1)
        self.pageControll.currentPage = page
    }
}
