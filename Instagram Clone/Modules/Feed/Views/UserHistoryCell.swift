//
//  UserHistoryCell.swift
//  Instagram Clone
//
//  Created by Alexei Pineda Caraballo on 30/08/2020.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit
import SDWebImage
class UserHistoryCell: UICollectionViewCell {

    @IBOutlet weak var newHistoryButton: UIButton!
    @IBOutlet weak var gradientImageView: UIImageView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var useerNameLabel: UILabel!

    func bind(viewModel:UserHistoryCellViewModel){
        self.useerNameLabel.text = viewModel.userName
        self.newHistoryButton.isHidden = !viewModel.currentUser
        self.gradientImageView.isHidden = viewModel.currentUser
        self.userImageView.sd_setImage(with: URL(string: viewModel.userDisplayUrl)) { (image, error, cache, url) in
            
        }
       
    }

}
