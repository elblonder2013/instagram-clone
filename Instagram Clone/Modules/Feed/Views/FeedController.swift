//
//  FeedController.swift
//  Instagram Clone
//
//  Created by Developer on 26/08/2020.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit

class FeedController: UITableViewController {
    @IBOutlet weak var feedTableView:UITableView!
    var viewModel  = FeedViewModel(manager: LocalFeedRepository())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        
        self.tableView.register(UINib(nibName: "PostCell", bundle: nil), forCellReuseIdentifier: "PostCellId")
        
        self.tableView.register(UINib(nibName: "HistoriesCell", bundle: nil), forCellReuseIdentifier: "HistoriesCellId")
        observeEvents()
        viewModel.loadFeed()
    }
    
    func observeEvents(){
        viewModel.loadFeedSuccesBlock = {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.tableView.reloadData()
            }
        }
    }
    
   
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "HistoriesCellId") as?  HistoriesCell else {return UITableViewCell()}
          
            cell.bind(viewModel: viewModel.viewModelForHistories())
           
            return cell
        } else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PostCellId") as?  PostCell else {return UITableViewCell()}
            cell.bind(viewModel: viewModel.viewModelForPostAtIndex(index: indexPath.row))
            return cell
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 1 : viewModel.postsLenght
    }
    
    
}



