//
//  FeedRepository.swift
//  Instagram Clone
//
//  Created by Developer on 26/08/2020.
//  Copyright © 2020 Developer. All rights reserved.
//


import UIKit
protocol FeedRepository {
    func getFeedsAndHistoriesForHome( completion: ([Post]?,[History]?,_ error:String?)->())
}

class LocalFeedRepository:FeedRepository{
    func getFeedsAndHistoriesForHome(completion: ([Post]?, [History]?, String?) -> ()) {
        
        let url = Bundle.main.url(forResource: "Posts", withExtension: "json")!
        let urlHistories = Bundle.main.url(forResource: "Histories", withExtension: "json")!
        let data = try! Data(contentsOf: url)
        let dataHistories = try! Data(contentsOf: urlHistories)
        let decoder = JSONDecoder()
        do {
            let posts =  try  decoder.decode([Post].self, from: data)
            let histories =  try  decoder.decode([History].self, from: dataHistories)
            completion(posts,histories,nil)
        } catch  {
            let error = error
            print(error.localizedDescription)
            completion(nil,nil, "Algo fue mal")
        }
        
        
        
        /*
         var feed1 = Post(postId: "1334", isVideo: false, mediaList: [], postCaption: "Esta es una pubilcacion de prueba", usernameAuthor: "kruding.com", profilePicUrl: "https://scontent-mad1-1.cdninstagram.com/v/t51.2885-19/s320x320/118288715_730083487848533_605153727897411789_n.jpg?_nc_ht=scontent-mad1-1.cdninstagram.com&_nc_ohc=8699752Xp1YAX8bYgjS&oh=92c44a322401874456bdca0be43600aa&oe=5F74E31B", likesCount: 200, commentsCount: 20, createdAt: 1598736351, displayUrl: "https://scontent-mad1-1.cdninstagram.com/v/t51.2885-15/e35/s1080x1080/118489518_194156845438697_4927165194016401383_n.jpg?_nc_ht=scontent-mad1-1.cdninstagram.com&_nc_cat=105&_nc_ohc=EeiihJAHOrcAX-jRzDZ&oh=cf4abf71e2f92d24c8c3141aceb83910&oe=5F73D09A", width: 1080, height: 1080)
         feed1.mediaList = [Media(mediaId: "asd", isVideo: false, width: 1080, height: 1080, urlSource: "https://scontent-mad1-1.cdninstagram.com/v/t51.2885-15/e35/s1080x1080/118489518_194156845438697_4927165194016401383_n.jpg?_nc_ht=scontent-mad1-1.cdninstagram.com&_nc_cat=105&_nc_ohc=EeiihJAHOrcAX-jRzDZ&oh=cf4abf71e2f92d24c8c3141aceb83910&oe=5F73D09A", displayUrl: "https://scontent-mad1-1.cdninstagram.com/v/t51.2885-15/sh0.08/e35/s640x640/118489518_194156845438697_4927165194016401383_n.jpg?_nc_ht=scontent-mad1-1.cdninstagram.com&_nc_cat=105&_nc_ohc=EeiihJAHOrcAX-jRzDZ&oh=904272c57f55f6d9960342a7342d2c97&oe=5F72A55E")]
         feed1.mediaList.append(Media(mediaId: "", isVideo: false, width: 2250, height: 1500, urlSource: "https://images.pexels.com/photos/97082/weimaraner-puppy-dog-snout-97082.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260", displayUrl: "https://images.pexels.com/photos/97082/weimaraner-puppy-dog-snout-97082.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"))
         
         var feed2 = Post(postId: "1334", isVideo: true, mediaList: [], postCaption: "Esta es una pubilcacion de video", usernameAuthor: "kruding.com", profilePicUrl: "https://scontent-mad1-1.cdninstagram.com/v/t51.2885-19/s320x320/118288715_730083487848533_605153727897411789_n.jpg?_nc_ht=scontent-mad1-1.cdninstagram.com&_nc_ohc=8699752Xp1YAX8bYgjS&oh=92c44a322401874456bdca0be43600aa&oe=5F74E31B", likesCount: 200, commentsCount: 20, createdAt: 1598736351, displayUrl: "https://i.pinimg.com/videos/thumbnails/originals/77/4f/21/774f219598dde62c33389469f5c1b5d1-00001.jpg", width: 720, height: 720)
         feed2.mediaList = [Media(mediaId: "sdfsd", isVideo: true, width: 720, height: 720, urlSource: "https://v.pinimg.com/videos/720p/77/4f/21/774f219598dde62c33389469f5c1b5d1.mp4", displayUrl: "https://i.pinimg.com/videos/thumbnails/originals/77/4f/21/774f219598dde62c33389469f5c1b5d1-00001.jpg")]
         completion([feed1,feed2],[],nil)*/
    }
}

