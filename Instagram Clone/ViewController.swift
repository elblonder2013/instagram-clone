//
//  ViewController.swift
//  Instagram Clone
//
//  Created by Developer on 26/08/2020.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var directContainerView: UIView!
    @IBOutlet weak var feedContainerView: UIView!
    @IBOutlet weak var historiesContainerView: UIView!
    @IBOutlet weak var mainScrollView: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
        feedContainerView.frame.origin.x = self.mainScrollView.frame.size.width
        directContainerView.frame.origin.x = self.mainScrollView.frame.size.width*2
        mainScrollView.contentSize = CGSize(width: self.view.frame.width * 3,height: 0)
        mainScrollView.contentOffset = CGPoint(x: self.view.frame.size.width, y: 0)
        mainScrollView.isPagingEnabled = true
    }
    
    
}

