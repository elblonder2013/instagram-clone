import UIKit
extension Date {
    
    init(milliseconds:Int) {
        let dateVar = Date.init(timeIntervalSinceNow: TimeInterval(milliseconds)/1000)
        self = dateVar
    }
   
    
    func relativePast() -> String {
        let date  = self
        let units = Set<Calendar.Component>([.year, .month, .day, .hour, .minute, .second, .weekOfYear])
        let components = Calendar.current.dateComponents(units, from: date, to: Date())
        if components.year! > 0 || components.month! > 0  || components.weekOfYear! > 0{
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "dd  MMMM  yyyy"
            return dateFormatterGet.string(from: self)
        } else if (components.day! > 0) {
            return (components.day! > 1 ? " Hace \(components.day!)  días" : "Ayer")
        } else if components.hour! > 0 {
            return "Hace \(components.hour!) " + (components.hour! == 1 ? " hora":" hora")
        } else if components.minute! > 0 {
            return "Hace \(components.minute!) " + (components.minute! == 1 ? " minuto":" minutos")
            
        } else {
            return "Hace \(components.second!) " + (components.second! == 1 ? " segundo":" segundos")
        }
    }
    
}
