//
//  TabBarController.swift
//  Instagram Clone
//
//  Created by Developer on 26/08/2020.
//  Copyright © 2020 Developer. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //configurando el tabbar
        self.tabBar.unselectedItemTintColor = .black
        
        //configurando el navigationBar
        setupLeftNavItem()
        setupRightNavItems()
        setupCenterNavItems()
        
        //creando los controladores
        let tabFeed = self.storyboard?.instantiateViewController(withIdentifier: "FeedController") as! FeedController
        let tabFeedBarItem = UITabBarItem(title: nil, image: #imageLiteral(resourceName: "home_inactived"), selectedImage: #imageLiteral(resourceName: "home_actived"))
        tabFeed.tabBarItem = tabFeedBarItem
        
        let tabSearch = SearchController()
        let tabSearchBarItem = UITabBarItem(title: nil, image: #imageLiteral(resourceName: "search_inactive"), selectedImage: #imageLiteral(resourceName: "search_actived"))
        tabSearch.tabBarItem = tabSearchBarItem
        self.viewControllers = [tabFeed,tabSearch]
        
        let newPostSearch = UIViewController()
        let newPostBarItem = UITabBarItem(title: nil, image: #imageLiteral(resourceName: "new_post"), selectedImage: #imageLiteral(resourceName: "new_post"))
        newPostSearch.tabBarItem = newPostBarItem
        self.viewControllers = [tabFeed,tabSearch,newPostSearch]
        
        let tabLikes = LikesController()
        let likesBarItem = UITabBarItem(title: nil, image: #imageLiteral(resourceName: "love_inactive"), selectedImage: #imageLiteral(resourceName: "love_active"))
        tabLikes.tabBarItem = likesBarItem
        
        let tabProfile = ProfileController()
               let profileBarItem = UITabBarItem(title: nil, image: #imageLiteral(resourceName: "profile_inactive"), selectedImage: #imageLiteral(resourceName: "profile_active"))
               tabProfile.tabBarItem = profileBarItem
        self.viewControllers = [tabFeed,tabSearch,newPostSearch,tabLikes,tabProfile]
        
        
      //  let vc = NewPostTabBarController()
       // self.present(vc, animated: true, completion: nil)
        
    }
    private func setupCenterNavItems() {
        let titleImageView = UIImageView(image: #imageLiteral(resourceName: "Instagram_logo"))
        titleImageView.frame = CGRect(x: 0, y: 0, width: 103, height: 29)
        titleImageView.contentMode = .scaleAspectFit
        navigationItem.titleView = titleImageView
        navigationController?.navigationBar.backgroundColor = .white
        navigationController?.navigationBar.isTranslucent = false
    }
    
    private func setupLeftNavItem() {
        let followButton = UIButton(type: .system)
        followButton.setImage(#imageLiteral(resourceName: "camera").withRenderingMode(.alwaysOriginal), for: .normal)
        followButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: followButton)
    }
    
    private func setupRightNavItems() {
        let igtvButton = UIButton(type: .system)
        igtvButton.setImage(#imageLiteral(resourceName: "igtv_icon").withRenderingMode(.alwaysOriginal), for: .normal)
        igtvButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        
        let directButton = UIButton(type: .system)
        directButton.setImage(#imageLiteral(resourceName: "send_message").withRenderingMode(.alwaysOriginal), for: .normal)
        directButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        
        navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: directButton), UIBarButtonItem(customView: igtvButton)]
    }
    
    
    
    
    
}
